# romulus (g5) qemu i2c configuration 

    /* The romulus board expects Epson RX8900 I2C RTC but a ds1338 is
     * good enough */
    /* i2c  1 bus*/
    /* i2c  2 bus*/
    i2c_slave_create_simple(aspeed_i2c_get_bus(&soc->i2c, 2), "pca9546", 0x74);
    i2c_slave_create_simple(aspeed_i2c_get_bus(&soc->i2c, 2), "pca9548", 0x77);
    /* i2c  3 bus*/
    i2c_slave_create_simple(aspeed_i2c_get_bus(&soc->i2c, 3), "pca9552", 0x60);
    /* i2c  4 bus*/
    /* i2c  5 bus*/
    /* i2c  6 bus*/
    /* i2c  7 bus*/
    /* The swift board expects a TMP275 but a TMP105 is compatible */
    i2c_slave_create_simple(aspeed_i2c_get_bus(&soc->i2c, 7), "tmp105", 0x48);
    /* The swift board expects a pca9551 but a pca9552 is compatible */
    i2c_slave_create_simple(aspeed_i2c_get_bus(&soc->i2c, 7), "pca9552", 0x60);

    /* i2c  8 bus*/
    /* The swift board expects an Epson RX8900 RTC but a ds1338 is compatible */
    i2c_slave_create_simple(aspeed_i2c_get_bus(&soc->i2c, 8), "ds1338", 0x32);
    i2c_slave_create_simple(aspeed_i2c_get_bus(&soc->i2c, 8), "pca9552", 0x60);

    /* i2c  9 bus*/
    i2c_slave_create_simple(aspeed_i2c_get_bus(&soc->i2c, 9), "tmp423", 0x4c);
    /* The swift board expects a pca9539 but a pca9552 is compatible */
    i2c_slave_create_simple(aspeed_i2c_get_bus(&soc->i2c, 9), "pca9552", 0x74);

    /* i2c 10 bus*/
    i2c_slave_create_simple(aspeed_i2c_get_bus(&soc->i2c, 10), "tmp423", 0x4c);
    /* The swift board expects a pca9539 but a pca9552 is compatible */
    i2c_slave_create_simple(aspeed_i2c_get_bus(&soc->i2c, 10), "pca9552",0x74);

    /* i2c 11 bus*/
    i2c_slave_create_simple(aspeed_i2c_get_bus(&soc->i2c, 11), "ds1338", 0x32);

    /* i2c 12 bus*/
    /* The swift board expects a TMP275 but a TMP105 is compatible */
    i2c_slave_create_simple(aspeed_i2c_get_bus(&soc->i2c, 12), "tmp105", 0x48);
    i2c_slave_create_simple(aspeed_i2c_get_bus(&soc->i2c, 12), "tmp105", 0x4a);
