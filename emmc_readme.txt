
### witherspoon-tacoma steps

../prepare-emmc-qemu witherspoon-tacoma  ~/op_latest/build_tacoma 16G

../qemu_latest/qemu-system-arm -m 1G -M tacoma-bmc -nographic -drive file=mmc-witherspoon-tacoma.img,format=raw,if=sd,index=2

####  login 
Phosphor OpenBMC (Phosphor OpenBMC Project Reference Distro) nodistro.0 witherspoon-tacoma ttyS4

witherspoon-tacoma login: root
Password:
root@witherspoon-tacoma:~# obmcutil state
CurrentBMCState     : xyz.openbmc_project.State.BMC.BMCState.Quiesced
CurrentPowerState   : xyz.openbmc_project.State.Chassis.PowerState.Off
CurrentHostState    : xyz.openbmc_project.State.Host.HostState.Off
BootProgress        : xyz.openbmc_project.State.Boot.Progress.ProgressStages.Unspecified
OperatingSystemState: xyz.openbmc_project.State.OperatingSystem.Status.OSStatus.Inactive
#### ---------------------------------------------------------------------








#  old  witherspoon-tacoma emmc qemu usage 

wget https://jenkins.openbmc.org/view/latest/job/latest-master/label=docker-builder,target=witherspoon-tacoma/lastSuccessfulBuild/artifact/openbmc/build/tmp/deploy/images/witherspoon-tacoma/aspeed-bmc-opp-tacoma.dtb
wget https://jenkins.openbmc.org/view/latest/job/latest-master/label=docker-builder,target=witherspoon-tacoma/lastSuccessfulBuild/artifact/openbmc/build/tmp/deploy/images/witherspoon-tacoma/fitImage-linux.bin-witherspoon-tacoma
wget https://jenkins.openbmc.org/view/latest/job/latest-master/label=docker-builder,target=witherspoon-tacoma/lastSuccessfulBuild/artifact/openbmc/build/tmp/deploy/images/witherspoon-tacoma/obmc-phosphor-image-witherspoon-tacoma.wic.xz

unxz obmc-phosphor-image-witherspoon-tacoma.wic.xz
truncate -s 16G obmc-phosphor-image-witherspoon-tacoma.wic
qemu-img convert -c -O qcow2 obmc-phosphor-image-witherspoon-tacoma.wic obmc-phosphor-image-witherspoon-tacoma.wic.qcow2


qemu-system-arm -M tacoma-bmc -nic user,hostfwd=::2222-:22,tftp=/srv/tftp/ -nographic -drive file=obmc-phosphor-image-witherspoon-tacoma.wic.qcow2,if=sd,index=2 -snapshot  -kernel fitImage-linux.bin-witherspoon-tacoma -dtb aspeed-bmc-opp-tacoma.dtb -append "console=ttyS4,115200n8 root=PARTLABEL=rofs-a"
