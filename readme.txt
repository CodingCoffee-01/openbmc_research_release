


wget https://jenkins.openbmc.org/view/latest/job/latest-master/label=docker-builder,target=romulus/lastSuccessfulBuild/artifact/openbmc/build/tmp/deploy/images/romulus/obmc-phosphor-image-romulus.static.mtd

wget https://jenkins.openbmc.org/view/latest/job/latest-master/label=docker-builder,target=gsj/lastSuccessfulBuild/artifact/openbmc/build/tmp/deploy/images/gsj/obmc-phosphor-image-gsj-20220713025106.static.mtd


./qemu-system-arm -m 512 -M romulus-bmc -nographic -drive file=./obmc-phosphor-image-romulus.static.mtd,format=raw,if=mtd -net nic -net user

./qemu-system-arm -m 512 -M tacoma-bmc -nographic -drive file=./obmc-phosphor-image-romulus.static.mtd,format=raw,if=mtd -net nic -net user

./qemu-system-arm -machine quanta-gsj -nographic  -drive file=image-bmc,if=mtd,bus=0,unit=0,format=raw
