    /* Bus 3: TODO bmp280@77 */
    /* Bus 3: TODO max31785@52 */
    dev = DEVICE(i2c_slave_new(TYPE_PCA9552, 0x60));
    qdev_prop_set_string(dev, "description", "pca1");
    i2c_slave_realize_and_unref(I2C_SLAVE(dev),
                                aspeed_i2c_get_bus(&soc->i2c, 3),
                                &error_fatal);

    for (size_t i = 0; i < ARRAY_SIZE(pca1_leds); i++) {
        led = led_create_simple(OBJECT(bmc),
                                pca1_leds[i].gpio_polarity,
                                pca1_leds[i].color,
                                pca1_leds[i].description);
        qdev_connect_gpio_out(dev, pca1_leds[i].gpio_id,
                              qdev_get_gpio_in(DEVICE(led), 0));
    }

    i2c_slave_create_simple(aspeed_i2c_get_bus(&soc->i2c, 3), "ibm-cffps",
        0x68);
    i2c_slave_create_simple(aspeed_i2c_get_bus(&soc->i2c, 3), "ibm-cffps",
        0x69);
    i2c_slave_create_simple(aspeed_i2c_get_bus(&soc->i2c, 3), "dps310", 0x76);

    i2c_slave_create_simple(aspeed_i2c_get_bus(&soc->i2c, 4), "tmp423", 0x4c);
    i2c_slave_create_simple(aspeed_i2c_get_bus(&soc->i2c, 4), "ir35221", 0x70);
    i2c_slave_create_simple(aspeed_i2c_get_bus(&soc->i2c, 4), "ir35221", 0x71);

    i2c_slave_create_simple(aspeed_i2c_get_bus(&soc->i2c, 5), "tmp423", 0x4c);
    i2c_slave_create_simple(aspeed_i2c_get_bus(&soc->i2c, 5), "ir35221", 0x70);
    i2c_slave_create_simple(aspeed_i2c_get_bus(&soc->i2c, 5), "ir35221", 0x71);



    i2c_slave_create_simple(aspeed_i2c_get_bus(&soc->i2c, 7), TYPE_LM75, 0x48);
    i2c_slave_create_simple(aspeed_i2c_get_bus(&soc->i2c, 7), TYPE_LM75, 0x49);
    i2c_slave_create_simple(aspeed_i2c_get_bus(&soc->i2c, 7), TYPE_LM75, 0x4a);
    i2c_slave_create_simple(aspeed_i2c_get_bus(&soc->i2c, 7), TYPE_LM75, 0x4b);
    i2c_slave_create_simple(aspeed_i2c_get_bus(&soc->i2c, 7), TYPE_LM75, 0x4c);
    i2c_slave_create_simple(aspeed_i2c_get_bus(&soc->i2c, 7), TYPE_LM75, 0x4d);
    i2c_slave_create_simple(aspeed_i2c_get_bus(&soc->i2c, 7), TYPE_LM75, 0x4e);
    i2c_slave_create_simple(aspeed_i2c_get_bus(&soc->i2c, 7), TYPE_LM75, 0x4f);

    /* The Witherspoon expects a TMP275 but a TMP105 is compatible */
    i2c_slave_create_simple(aspeed_i2c_get_bus(&soc->i2c, 9), TYPE_TMP105,
                     0x4a);

    /* The witherspoon board expects Epson RX8900 I2C RTC but a ds1338 is
     * good enough */
    i2c_slave_create_simple(aspeed_i2c_get_bus(&soc->i2c, 11), "ds1338", 0x32);

    smbus_eeprom_init_one(aspeed_i2c_get_bus(&soc->i2c, 11), 0x51,
                          eeprom_buf);
    dev = DEVICE(i2c_slave_new(TYPE_PCA9552, 0x60));
    qdev_prop_set_string(dev, "description", "pca0");
    i2c_slave_realize_and_unref(I2C_SLAVE(dev),
                                aspeed_i2c_get_bus(&soc->i2c, 11),
                                &error_fatal);
